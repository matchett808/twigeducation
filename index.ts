/*
** Some rough bootstrapping to allow us to run this from the CLI
** Left here for ease
*/
/*
const CLIArgs = process.argv.slice(2);
const TheArrayToSplit = JSON.parse(CLIArgs[0]);
const Segments = JSON.parse(CLIArgs[1]);
*/

/*
**
** Function The splits arrays based on a given input segment
**
*/
export function ArraySplitter(InputArray: Array<any>, Segments: number): Array<any> {
    if (Segments < 0) {
        throw new Error('Segments cannot be negative');
    }
    if (typeof(Segments) !== 'number') {
        throw new Error('Segments must be a number');
    }
    const chunkSize = Math.floor((InputArray.length/Segments)+0.99); // Compute chunk size based on array size and segments
    let output = [];
    for (let i = 0; i < InputArray.length; i+= chunkSize){
        output.push(InputArray.slice(i,i+chunkSize));
    }
    return output;
}