
import { ArraySplitter } from './index.js';
import { assert, expect } from 'chai';
import 'mocha';

describe('Array Splitter Function', () => {

  it('It should return an array of arrays', () => {
    const result = ArraySplitter([1,2,3,4], 2);
    expect(result).eql([[1,2],[3,4]]);
  });

  it('It should return an empty array if given an empty array', () => {
    const result = ArraySplitter([], 1);
    expect(result).eql([]);
  });

  it('It should return the original array if given 1 segment', () => {
    const result = ArraySplitter([1,2,3,4], 1);
    expect(result).eql([[1,2,3,4]]);
  });

  it('It should return the expected values', () => {
    const result = ArraySplitter([1, 2, 3, 4, 5], 3);
    expect(result).eql([ [ 1, 2 ], [ 3, 4 ], [ 5 ] ]);
  });
  it('It should throw an error for negative segments', () => {
    try 
    {
        expect(ArraySplitter([1, 2, 3, 4, 5], -3)).to.throw();
    } 
    catch (err) 
    {
        expect(err);
    }
  });
  it('It should throw an error for a string rather than segments', () => {
    try 
    {
        expect(ArraySplitter([1, 2, 3, 4, 5], "NOT A NUMBER")).to.throw();
    } 
    catch (err) 
    {
        expect(err);
    }
  });
});